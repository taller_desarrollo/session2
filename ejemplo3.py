from fastapi import FastAPI, Request
from fastapi.responses import FileResponse

app = FastAPI()

@app.get("/ejemplo5")
async def read_form():
    return FileResponse("pagina2.html")

@app.post("/calcular-cuadrado")

async def calcular_cuadrado(request: Request):
    
    # Obtén el número enviado por el formulario
    datos_formulario = await request.form()
    numero = int(datos_formulario["numero"])
    
    # Calcula el cuadrado del número
    cuadrado = numero ** 2
    
    # Devuelve el resultado
    return {"cuadrado": cuadrado}

